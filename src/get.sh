#!/usr/bin/env bash

git submodule init

git submodule add https://github.com/kkoojjyy/vagrant-libvirt.git
git submodule add  https://github.com/kkoojjyy/vagrant-docker-compose.git

git submodule update